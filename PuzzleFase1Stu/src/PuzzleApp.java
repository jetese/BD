import view.PuzzleGUI;
import model.AbstractModel;
import model.*;
import control.*;
import view.*;
import config.*;
/*
 * Copyright 2016 Miguel Ã�ngel RodrÃ­guez-GarcÃ­a (miguel.rodriguez@urjc.es).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Clase principal que ejecuta el juego
 * @Author Miguel Ã�ngel
 * @version 1.0
 */
public class PuzzleApp {

    public static void main(String args[]){
    	
    	//Cargamos la configuración inicial del archivo xml
    	Config con = new Config();
    	con = ConfigManagement.getConfiguration();
    	
        int imageSize = con.getImageSize();
        int rowNum = con.getNumFilas();
        int columnNum= con.getNumColum();
        
        
        String[] imageList= ConfigManagement.getUrl();
        int bd = ConfigManagement.getBaseDatos();

        // Creamos el modelo
        AbstractModel modelo = new Model(rowNum, columnNum, imageSize, imageList);

        // Creamos el controlador
        AbstractController controlador = new Controller (bd,rowNum, columnNum, imageSize, imageList);
        
        // Inicializamos la GUI
        PuzzleGUI.initialize(controlador, rowNum, columnNum, imageSize, imageList);
        
        // Obtenemos la vista del tablero
        BoardView vistaTablero = PuzzleGUI.getInstance().getBoardView();
        
        // Añadimos un nuevo observador al controlador
        controlador.addObserver(modelo);
        controlador.addObserver(vistaTablero);

        // Visualizamos la aplicaciÃ³n.
        PuzzleGUI.getInstance().setVisible(true);
    }
}
