package control;


import view.InfoView;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.File;


import javax.swing.WindowConstants;

import model.AbstractModel;
import model.Model;
import observer.Observer;
import command.ConcreteCommand;
import config.ConfigManagement;

import datos.BaseDatos;
import datos.ConcreteMongoDB;
import datos.ConcreteXml;
//import javafx.scene.web.HTMLEditorSkin.Command;
import view.PuzzleGUI;

/*	Clase que se encarga de los eventos de la interfaz
 * 	y de notificar a los observadores las piezas que tienen
 * 	que intercambiar	*/
public class Controller extends AbstractController {
	ConcreteCommand comando;
	private int lastPos = 0;
	private int numVecesDesordenar = 20;
	private int bdType = 0;
	private BaseDatos base;
	final int NONE = 0;
	final int MONGO = 1;
	final int XML = 2;
	
	//Inicializamos el controller con la base de datos que nos viene del Xml y nos conectamos a ella
	//borrando la informaci�n que contenga y cargando la configuraci�n inicial
	public Controller(int bd,int rowNum, int colNum, int imageSize, String[]imageList) {
		super();
		bdType = bd;
		switch(bdType) {
			case MONGO:
				base = new ConcreteMongoDB();
				base.crearBaseDatos();
				base.delete();
				base.guardarConfig(colNum, rowNum, imageSize, imageList);
				break;
			case XML:
				bdType = XML;
				base = new ConcreteXml();
				base.crearBaseDatos();
				base.delete();
				base.guardarConfig(colNum, rowNum, imageSize, imageList);
				break;
			case NONE:
				bdType = NONE;
				break;
			default:
				System.out.println("Base de datos incorrecta");
				break;
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(arg0.getActionCommand()) {
			case "info":
				new InfoView();
				break;
			case "exit":
				salir();
				break;
			case "load":
				cargarPartida();
				bdType = NONE;
				break;
			case "save":
				guardarPartida();
				break;
			case "solve":
				solvePuzzle();
				break;
			case "clutter":
				desordenar();
				break;
			case "Img Config":
				cargarImagen();
				break;
			case "Mongo DB":
				cargarMongo();
				break;
			case "XML":
				cargarXml();
				break;
			case "Statistics":
				if (bdType != NONE) {
					calcularEstadisticas();
				}
				else {
					PuzzleGUI.getInstance().messagePanel("No hay estadisticas para este modo");
				}
				break;
		}
		
	}

	/*Intercambia dos casillas en todos los observadores*/
	@Override
	public void notifyObservers(int blankPos, int movedPos) {
		// TODO Auto-generated method stub
		if(blankPos != -1) {
			
			for (int i = 0; i<super.observerList.size();i++) {
				observerList.get(i).update(blankPos, movedPos);
			}
		}
		if(getModel().isPuzzleSolve() && blankPos != -1) {
			PuzzleGUI.getInstance().messagePanel("Has ganado");
		}
	}
	
	/*Cuando se hace click, obtiene la posici�n del rat�n
	 * obtiene las piezas que se van a mover en el caso de que 
	 * sea una pieza que colinde con la blanca, ejecuta el comando
	 * para mover las piezas y agrega el comando a la lista de comandos*/
	@Override
	public void mousePressed(MouseEvent e) {
		Model mod = getModel();
		mod.setDesordenado(false);
		super.mousePressed(e);
		int x=e.getX();
	    int y=e.getY();
	    int [] piezas = PuzzleGUI.getInstance().getBoardView().movePiece(x,y);
	    comando = new ConcreteCommand(piezas);
	    comando.redoCommand();
	    if(bdType == NONE) {
	    	commandsList.add(comando);
	    }
	    else {
	    	base.guardarMovimiento(comando);
	    }
	}
	
	/*	Resuelve el puzle	*/
	private void solvePuzzle() {
		Model mod = getModel();
		mod.setDesordenado(true);
		
		if (bdType == NONE) {
			for (int i =commandsList.size()-1; i>=0; i--) {
				commandsList.pollLast().undoCommand();			
			}
		}
		else {
			//base.count
			int numMovimientos = base.getSize();
			for(int i =0; i<numMovimientos; i++) {
				base.recuperarMovimiento().undoCommand();
			}
		}
	}
	
	/*Desordena las piezas*/
	private void desordenar() {
		Model mod = getModel();
		mod.setDesordenado(true);
		for (int i = 0; i< numVecesDesordenar; i++) {
			int [] rndPosition = new int [2];
			rndPosition = mod.getRandomMovement(lastPos,0);
			lastPos = rndPosition[1];

			comando = new ConcreteCommand(rndPosition);
		    comando.redoCommand();
		    
		    if(bdType == NONE) {
		    	commandsList.add(comando);
		    }
		    else {
		    	base.guardarMovimiento(comando);
		    }
		}
	}
	
	/*Guarda partida en json*/
	private void guardarPartida() {
		if(bdType != 0) {
			commandsList = base.cargarPartida();
		}
		ConfigManagement.guardarConfiguracion(commandsList);
	}
	/* Carga partida en json*/
	private void cargarPartida() {

		commandsList = ConfigManagement.cargarPartida();
		PuzzleGUI.getInstance().setParams(ConfigManagement.getFilas(),ConfigManagement.getColumnas(),ConfigManagement.getSize(),ConfigManagement.getUrl());
		
		PuzzleGUI.getInstance().updateBoardLoad();
		AbstractModel modelo =getModel(); 
		removeObserver(modelo);
		modelo = new Model(PuzzleGUI.getInstance().rowNum,PuzzleGUI.getInstance().columnNum,PuzzleGUI.getInstance().imageSize,PuzzleGUI.getInstance().imageList);
		addObserver(modelo);
		Model mod = getModel();
		mod.setDesordenado(true);
		
		for (int i = 0; i<commandsList.size();i++ ) {
			commandsList.get(i).redoCommand();	
		}
		if(base!=null) {
			base.cerrar();
		}
	}
	
	
	/* Carga la imagen */
	private void cargarImagen() {

		File imageFile = PuzzleGUI.getInstance().showFileSelector();
		PuzzleGUI.getInstance().updateBoard(imageFile);
		AbstractModel modelo =getModel(); 
		removeObserver(modelo);
		modelo = new Model(PuzzleGUI.getInstance().rowNum,PuzzleGUI.getInstance().columnNum,PuzzleGUI.getInstance().imageSize);
		addObserver(modelo);
		if(bdType != NONE) {
			base.guardarConfig(PuzzleGUI.getInstance().columnNum,PuzzleGUI.getInstance().rowNum,PuzzleGUI.getInstance().imageSize,PuzzleGUI.getInstance().imageList);
		}
	}
	
	/* Salir */
	private void salir() {
		System.exit(WindowConstants.EXIT_ON_CLOSE);
	}
	
	/* Obtener el observer model*/
	private Model getModel(){
		
		Model ret = null;
		for(Observer o:observerList){
			if(o instanceof Model){
				ret = (Model)o;
			}
		}
		
		return ret;
	}
	
	//Funcion que nos conecta a MongoDB
	private void cargarMongo() {
		bdType = MONGO;
		if(base!=null) {
			base.cerrar();
		}
		base = new ConcreteMongoDB();
		base.crearBaseDatos();
		System.out.println("Elegido modo Mongo");
		if(!base.hayConfig()) {
			base.guardarConfig(PuzzleGUI.getInstance().columnNum,PuzzleGUI.getInstance().rowNum,PuzzleGUI.getInstance().imageSize,PuzzleGUI.getInstance().imageList);
		}
		cargarBd();
	}
	
	//Funcion que nos conecta a la base de datos de Xml
	private void cargarXml() {
		bdType = XML;
		if(base!=null) {
			base.cerrar();
		}
		base = new ConcreteXml();
		base.crearBaseDatos();
		System.out.println("Elegido XML");
		if(!base.hayConfig()) {
			base.guardarConfig(PuzzleGUI.getInstance().columnNum,PuzzleGUI.getInstance().rowNum,PuzzleGUI.getInstance().imageSize,PuzzleGUI.getInstance().imageList);
		}
		cargarBd();
	}
	
	//Cargamos la configuraci�n y los movimientos de la base de datos 
		private void cargarBd() {
			commandsList = base.cargarPartida();
			
			int colNum = base.getColumn();
			int rowNum = base.getRow();
			int imageSize = base.getImageSize();
			String[] imageList = base.getImage();
			
			ConfigManagement.setConfiguration(rowNum, colNum, imageSize, imageList, bdType);
			PuzzleGUI.getInstance().setParams(rowNum, colNum,imageSize,imageList);
			
			PuzzleGUI.getInstance().updateBoardLoad();
			AbstractModel modelo =getModel(); 
			removeObserver(modelo);
			modelo = new Model(rowNum,colNum, imageSize,imageList);
			addObserver(modelo);
			Model mod = getModel();
			mod.setDesordenado(true);
			
			for (int i = 0; i<commandsList.size();i++ ) {
				commandsList.get(i).redoCommand();	
			}
		}
	
	//Funci�n que recibe las estad�sticas de la base de datos y las muestra
	private void calcularEstadisticas() {
		double[] times = base.tiempos();
		double memoria = base.memoria();
		double tiempoMedioInsercion = 0; 
		double tiempoMedioConsulta = 0;
		if(times[1]!= 0) {
			tiempoMedioInsercion = times[0]/times[1];
		}
		if(times[3]!= 0) {
			tiempoMedioConsulta = times[2]/times[3];
		}

		String mensaje =	"El tiempo total de inserci�n es de: "+times[0]+" ms\n"+
							"El tiempo medio de inserci�n es de: "+tiempoMedioInsercion+" ms\n"+
							"El n�mero de inserciones es de: "+times[1]+"\n\n"+
							"El tiempo total de consulta es de: "+times[2]+" ms\n"+							
							"El tiempo medio de consulta es de: "+tiempoMedioConsulta+" ms\n"+							
							"El n�mero de consultas es de: "+times[3]+"\n\n"+
							"La memoria ocupada es de: "+memoria+" B";
		PuzzleGUI.getInstance().messagePanel(mensaje);
	}
}
