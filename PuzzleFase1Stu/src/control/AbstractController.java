package control;

import observer.Observable;
import observer.Observer;
import command.Command;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.LinkedList;
/**
 * Interfaz que tiene que ser implementada por un controlador.
 * @author Miguel Ã�ngel
 * @version 1.0
 */
public abstract class AbstractController extends MouseAdapter implements ActionListener, Observable {
    protected ArrayList<Observer> observerList;
    protected LinkedList<Command> commandsList;
    public AbstractController(){
        observerList = new ArrayList<Observer>();
        commandsList = new LinkedList<Command>();
    }

    public void addObserver(Observer observer){
        if(observer!=null){
            observerList.add(observer);
        }
    }

    public void removeObserver(Observer observer){
        if(observer!=null){
            observerList.remove(observer);
        }
    }

}
