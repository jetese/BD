package command;



import view.PuzzleGUI;

//Clase que implementa Command
//Se encarga de los comandos a realizar


public class ConcreteCommand implements Command {
	//Array de la posici�n de las piezas a mover en el array de PieceModel y PieceView

	int[] piezas;
	int id=0;

	public ConcreteCommand() {
		
	}
	
	public ConcreteCommand(int[]piece) {
	    piezas = piece;
	}

	@Override
	public void undoCommand() {
	    PuzzleGUI.controller.notifyObservers(piezas[0],piezas[1]);
	}

	@Override
	public void redoCommand() {
	    PuzzleGUI.controller.notifyObservers(piezas[0],piezas[1]);
	}

	public int[] getPiezas() {
		return piezas;
	}

//	public void setPiezas(int[] piezas) {
//		this.piezas = piezas;
//	}
	
	public String toString() {
		return("element movimiento { attribute id {'" + id+"'},"+
								"element piezas{'" + piezas[0] +","+piezas[1]+ "'}}");
	}
}
