package datos;

import java.util.LinkedList;
import command.Command;

public interface BaseDatos {
	public void crearBaseDatos();
	public void guardarMovimiento(Command comando);
	public Command recuperarMovimiento();
	public LinkedList <Command> cargarPartida();
	public int getSize();
	public void delete();
	public void guardarConfig(int colum,int row,int imageSize,String [] image);
	public int getColumn();
	public int getRow();
	public int getImageSize();
	public String[] getImage();
	public void cerrar();
	public double[] tiempos();
	public int memoria();
	public boolean hayConfig();
}
