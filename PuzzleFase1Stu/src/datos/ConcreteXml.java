package datos;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;

import org.basex.core.BaseXException;
import org.basex.core.Context;
import org.basex.core.cmd.CreateDB;
import org.basex.core.cmd.InfoDB;
import org.basex.core.cmd.XQuery;

import command.Command;
import command.ConcreteCommand;



public class ConcreteXml implements BaseDatos {
	private Context contextoxml;
	private String path;
	private double saveTime;
	private double loadTime;
	private Date dateTime;
	private int loadCount;
	private int saveCount;
	public ConcreteXml() {
		saveTime = 0;
		loadTime = 0;
		loadCount = 0;
		saveCount = 0;
		dateTime = new Date();
	}
	@Override
	public void crearBaseDatos() {
		path = System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"Base XML"+System.getProperty("file.separator")+"base.xml";
		contextoxml = new Context();
		try {
			//Creamos la base de datos en memoria RAM accediendo al fichero XML ya definido
			new CreateDB("prueba",path).execute(contextoxml);
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Insertamos el nodo con las piezas en la base de datos y actualizamos el fichero XML
	 * 
	 */
	
	@Override
	public void guardarMovimiento(Command comando) {

		long time = System.currentTimeMillis(); 
		XQuery insert = new XQuery("insert node "+comando+"into /documento");
		try {
			insert.execute(contextoxml);
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateXml();
		saveTime += System.currentTimeMillis()-time;
		saveCount++;
		
	}

	/* Recuperamos el �ltimo movimiento de la base de datos para resolver el puzzle.
	 * Se recoge el �ltimo movimiento dentro de documento y se parsea a String.
	 * Posteriormente se elimina dicho nodo de la base de datos
	 * Con el String, debemos separarlo en un array para obtener las piezas, y adem�s realizar una conversi�n
	 * a entero.
	 */
	@Override
	public Command recuperarMovimiento() {
		String movimiento = "";
		long time =System.currentTimeMillis();
		
		XQuery cargar = new XQuery("documento/movimiento[last()]/piezas/text()");
		XQuery delete = new XQuery("delete node documento/movimiento[last()]");
		try {
			//Ejecutamos la query
			movimiento = cargar.execute(contextoxml);
			delete.execute(contextoxml);
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateXml();
		loadTime += System.currentTimeMillis()-time;
		loadCount++;
		if(movimiento!=null) {
			String [] cortar = movimiento.split(",");
			int[] movimientoFinal = {Integer.parseInt(cortar[0]),Integer.parseInt(cortar[1])};
			return new ConcreteCommand(movimientoFinal);
		}
		return null;
	}
	
	/*
	 * Actualiza el fichero xml en funci�n de los datos de memoria RAM
	 */
	
	private void updateXml() {
		try {
			XQuery serializeQ = new XQuery("for $item in /documento \n"+
											"return file:write('"+path + "',$item)");
			//Ejecutamos la query
			serializeQ.execute(contextoxml);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Se recuperan todos los movimientos de la base de datos y se crea una lista de comandos para cargar la partida.
	 * 
	 */
	
	@Override
	public LinkedList <Command> cargarPartida() {
		String datos ="";
		String [] movimientos;
		LinkedList <Command> comandos = new LinkedList<Command>();
		if(getSize()!=0) {
			XQuery todos = new XQuery("documento/movimiento/piezas/text()");
			
			try {
				//Ejecutamos la query
				datos = todos.execute(contextoxml);
			} catch (BaseXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			movimientos = datos.split("\r\n");
			for(String movement : movimientos) {
				String [] comando = movement.split(",");
				int [] aux = {Integer.parseInt(comando[0]),Integer.parseInt(comando[1])};
				comandos.add(new ConcreteCommand(aux));
			}
			
		}
		return comandos;
	}

	/*
	 * Obtenemos el n�mero de movimientos almacenados en la base de datos
	 * 
	 */
	
	@Override
	public int getSize() {
		int entradas = 0;
		XQuery cuenta = new XQuery("count(documento/movimiento)");
		try {
			//Ejecutamos la query
			entradas = Integer.parseInt(cuenta.execute(contextoxml));
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return entradas;
	}
	
	/*
	 * Eliminamos todo el contenido dentro del nodo raiz documento (configuraci�n incluida)
	 */
	
	@Override
	public void delete() {
		XQuery delete = new XQuery("delete node documento/movimiento");
		XQuery deleteConfig = new XQuery("delete node documento/config");
		try {
			//Ejecutamos las querys
			delete.execute(contextoxml);
			deleteConfig.execute(contextoxml);
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateXml();
	}

	/*
	 * Creamos el nodo configuraci�n con las filas, columnas, tama�o de imagen y ruta de las im�genes.
	 * Posteriormente se inserta en la base de datos y se actualiza el fichero
	 */
	
	@Override
	public void guardarConfig(int colum,int row,int imageSize,String [] image) {
		XQuery delete = new XQuery("delete node documento/config");
		String config = "element config {"+
				"element columnNum{" +colum+ "},element rowNum{" +row+"},element sizeImage{"+imageSize+"},"
				+"element imageList{'"+Arrays.toString(image)+"'}}";
						
		XQuery insert = new XQuery("insert node "+config+"into /documento");
		try {
			delete.execute(contextoxml);
			insert.execute(contextoxml);
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		updateXml();
	}

	/*
	 * Recuperamos las columnas de la base de datos
	 */
	
	@Override
	public int getColumn() {
		int columnNum = 0;
		XQuery column = new XQuery("documento/config/columnNum/text()");
		try {
			columnNum = Integer.parseInt(column.execute(contextoxml));
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return columnNum;
	}

	/*
	 * Recuperamos las filas de la base de datos
	 */
	@Override
	public int getRow() {
		int rowNum = 0;
		XQuery row = new XQuery("documento/config/rowNum/text()");
		try {
			rowNum = Integer.parseInt(row.execute(contextoxml));
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rowNum;
	}
	
	/*
	 * Recuperamos el tama�o de la imagen de la base de datos
	 */
	@Override
	public int getImageSize() {
		int sizeImage = 0;
		XQuery size = new XQuery("documento/config/sizeImage/text()");
		try {
			sizeImage = Integer.parseInt(size.execute(contextoxml));
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sizeImage;
	}

	/*
	 * Recuperamos la ruta de la imagen de la base de datos. Para ello, se debe eliminar del substring el espacio entre coma y ruta, as� como 
	 * los corchetes que se insertan por defecto al principio y final del string devuelto
	 */
	@Override
	public String[] getImage() {
		String largo = "";
		String [] url;
		XQuery image = new XQuery("documento/config/imageList/text()");
		try {
			largo = image.execute(contextoxml);
			String aux = largo.substring(1, largo.length()-1);
			url = aux.split(",\\s+");
			return url;
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * Cerramos la base de datos
	 */
	
	@Override
	public void cerrar() {
		contextoxml.close();
	}

	/*
	 * Devoluci�n de los tiempos para las estad�sticas
	 */
	
	@Override
	public double[] tiempos() {
		// TODO Auto-generated method stub
		double[] tiempos = new double[4];
		tiempos[0] = saveTime;
		tiempos[1] = saveCount;
		tiempos[2] = loadTime;
		tiempos[3] = loadCount;
		return tiempos;
	}

	/*
	 * Devuelve el tama�o del fichero XML, correspondiente al tama�o de la base de datos
	 */
	
	@Override
	public int memoria() {
		String memory = "";
		path = System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"Base XML"+System.getProperty("file.separator")+"base.xml";

		return (int)new File(path).length();
	}
	@Override
	public boolean hayConfig() {
		int entradas=0;
		XQuery cuenta = new XQuery("count(documento/config)");
		try {
			//Ejecutamos la query
			entradas = Integer.parseInt(cuenta.execute(contextoxml));
		} catch (BaseXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(entradas==0) {
			return false;
		}
		return true;
	}

}
