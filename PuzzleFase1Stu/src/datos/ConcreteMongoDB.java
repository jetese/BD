package datos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import command.Command;
import command.ConcreteCommand;

public class ConcreteMongoDB implements BaseDatos {
	private static MongoCollection<Document> coll;
	private static MongoCollection<Document> config;
	private MongoClient mongoClient;
	private double saveTime;
	private double loadTime;
	private Date dateTime;
	private int loadCount;
	private int saveCount;
	public ConcreteMongoDB() {
		saveTime = 0;
		loadTime = 0;
		loadCount = 0;
		saveCount = 0;
		dateTime = new Date();
	}
	/*
	 * Nos conectamos al servidor de mongo, conectamos con la bse de datos o la creamos si no existe y recuperamos la collection de movimientos
	 * y configuraci�n
	 */
	
	@Override
	public void crearBaseDatos() {
		// TODO Auto-generated method stub
		//Conectamos con el servidor de Mongo
		mongoClient = new MongoClient();
				
		//Conectamos con la base de datos o la creamos en caso de que no exista
		MongoDatabase db = mongoClient.getDatabase("mongoDB");
		
		//Creamos la colleci�n de movimientos
		if(db.getCollection("Partida")==null) {
			db.createCollection("Partida");
		}
		coll = db.getCollection("Partida");
		
		//Creamos la colleci�n de configuraci�n
		if(db.getCollection("Config")==null) {
			db.createCollection("Config");
		}
		config = db.getCollection("Config");
	}

	/*
	 * Se almacena un documento con las piezas por movimiento
	 */
	@Override
	public void guardarMovimiento(Command comando) {
		// TODO Auto-generated method stub
		int[] piezas = comando.getPiezas();
		Document documento = new Document();
		
		long time = System.currentTimeMillis(); 
	    //Insertamos en el documento una nueva entrada con la lista de piezas
	    documento.put("comando",Arrays.stream(piezas).boxed().collect(Collectors.toList()));
	    coll.insertOne(documento);
	    //Calculamos el tiempo y aumentamos el n�mero de iteraciones
	    saveTime += (System.currentTimeMillis()-time);
	    saveCount++;
	}

	/*
	 * Recuperamos el �ltimo movimiento de la lista y lo eliminamos, devolviento asi el comando con las piezas
	 */
	@Override
	public ConcreteCommand recuperarMovimiento() {
		// TODO Auto-generated method stub
		List <Integer> list = new ArrayList<Integer>();
		//Se recupera el �ltimo documento y se elimina
		long time = System.currentTimeMillis(); 
		Document last = coll.findOneAndDelete(coll.find().sort(new BasicDBObject("_id",-1)).first());
		loadTime += System.currentTimeMillis()-time;
		//Si la lista no esta vacia, reguperamos el �ltimo movimiento y devolvemos un concrete command con dicho movimiento
		if(last!=null) {
			list.addAll((ArrayList)last.get("comando"));
			int[] movimiento = {list.get(0),list.get(1)};
			loadCount++;
			return new ConcreteCommand(movimiento);
		}
		return null;
	}
	
	/*
	 * Recuperamos todos los documentos en orden para obtener los movimientos y poder cargar la partida,
	 *  devolviento una lista con los comandos
	 */
	@Override
	public LinkedList <Command> cargarPartida() {
		// TODO Auto-generated method stub
		List <Integer> list = new ArrayList<Integer>();
		//Se recogen todos los documentos y se itera uno por uno
		for(Document dock : coll.find()) {
			list.addAll((ArrayList)dock.get("comando"));
		}
		//Devolvemos una lista con todos los movimientos almacenados
		LinkedList <Command> comandos = new LinkedList<Command>();
		for(int i = 0; i< list.size();i+=2) {
			int []pieza = new int[2];
			pieza[0] = list.get(i);
			pieza[1] = list.get(i+1);
			comandos.add(new ConcreteCommand(pieza));
		}
		return comandos;
	}
	
	/*
	 * Obtenemos el n�mero de movimientos de la collection
	 */
	@Override
	public int getSize() {
		//Contamos los documentos almacenados en la colleccion de Movimientos
		return (int) coll.count();
	}
	
	/*
	 * Creamos un objeto documento en blanco y eliminamos todos los documentos de las collecciones
	 */
	@Override
	public void delete() {
		BasicDBObject documento = new BasicDBObject();
		coll.deleteMany(documento);
		config.deleteMany(documento);
		
	}
	
	/*
	 * Creamos un documento al que insertamos los datos necesarios para la configuraci�n de la partida
	 */
	@Override
	public void guardarConfig(int colum,int row,int imageSize,String [] image) {
		BasicDBObject documento = new BasicDBObject();
		config.deleteMany(documento);
		Document confiInfo = new Document()
				.append("columnNum", colum)
				.append("rowNum", row)
				.append("sizeImage", imageSize)
				.append("imageList", Arrays.asList(image));
		config.insertOne(confiInfo);
	}
	
	/*
	 * Recuperamos el n�mero de filas del documento
	 */
	@Override
	public int getColumn() {
		if(config.count()!=0) {
		Document column = config.find().first();
		return (int) column.get("columnNum");
		}
		return 0;
	}
	
	/*
	 * Recuperamos el n�mero de columnas del documento
	 */
	@Override
	public int getRow() {
		if(config.count()!=0) {
		Document row = config.find().first();
		return (int) row.get("rowNum");
		}
		return 0;
	}
	
	/*
	 * Recuperamos el tama�o de imagen del documento
	 */
	@Override
	public int getImageSize() {
		if(config.count()!=0) {
		Document imageSize = config.find().first();
		return (int) imageSize.get("sizeImage");
	}
	return 0;
	}
	
	/*
	 * Recuperamos la ruta de la imagen del documento
	 */
	@Override
	public String[] getImage() {
		if(config.count()!=0) {
		List <String> list = new ArrayList<String>();
		
		Document image = config.find().first();
		list.addAll((ArrayList)image.get("imageList"));
		
		String [] url = new String[list.size()];
		for(int i = 0;i<list.size();i++) {
			url[i] = list.get(i);
		}
		return url;
	}
	return null;
	}
	
	/*
	 * Desconectamos del servidor de mongo
	 */
	@Override
	public void cerrar() {
		mongoClient.close();
	}
	
	/*
	 * Devolvemos los tiempos calculados por inserci�n y consulta de base de datos
	 */
	@Override
	public double[] tiempos() {
		double[] tiempos = new double[4];
		tiempos[0] = saveTime;
		tiempos[1] = saveCount;
		tiempos[2] = loadTime;
		tiempos[3] = loadCount;
		return tiempos;
	}
	
	/*
	 *Calculamos el tama�o de la collection que almacena los movimientos 
	 */
	@Override
	public int memoria() {
		DB db = mongoClient.getDB("mongoDB");
		CommandResult cr= db.getCollection(coll.getNamespace().getCollectionName()).getStats();
		cr.get("size");
		return (int) cr.get("size");
	}

	@Override
	public boolean hayConfig() {
		if(config.count()==0) {
			return false;
		}
		return true;
	}
}
