package model;


/*Clase de cada pieza en el array de piezas de Model*/
public class PieceModel {

    //id de la imagen
    private int id;
    //índice de fila
    private int indexRow;
    //índice de columna
    private int indexColumn;
    //Tamaño de la imagen
    private int imageSize;


    /**
     * Constructor de una clase
     * @param indexRow indice de fila
     * @param indexColumn indice de columna
     * @param imagePath ubicación de la imagen.
     */
    public PieceModel(int id,int indexRow, int indexColumn,int imageSize,String imagePath){
        this.id = id;
        this.indexRow = indexRow;
        this.indexColumn = indexColumn;
        this.imageSize = imageSize;
    }

    public PieceModel(int id, int indexRow, int indexColumn,int imageSize){
    	this.id = id;
        this.indexRow = indexRow;
        this.indexColumn = indexColumn;
        this.imageSize = imageSize;
    }


    public int getIndexRow() {
        return indexRow;
    }
    
    public void setIndexRow(int row) {
    	this.indexRow = row;
    }

    public int getIndexColumn() {
        return indexColumn;
    }
    
    public void setIndexColumn(int col) {
    	this.indexColumn = col;
    }
    
    public void setColRow (int col, int row) {
    	setIndexRow(row);
    	setIndexColumn(col);
    }
    
    public int getDrawnRowIndex() {
    	return indexRow * imageSize;
    }
    public int getDrawnColumnIndex() {
    	return indexColumn * imageSize;
    }
    
    public int getImageSize() {
        return imageSize;
    }

    public void setImageSize(int imageSize) {
        this.imageSize = imageSize;
    }

    public int getId(){
        return this.id;
    }

    public String toString(){
        return("id:"+id);
    }

}
