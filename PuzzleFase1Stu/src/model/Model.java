package model;
import java.util.ArrayList;
import java.util.Random;


/*Clase que se encarga de las piezas y sus posiciones*/
public class Model extends AbstractModel{
	int[][] piezas ;
	private ArrayList<PieceModel> iconArray = null;
    private Random rnd = new Random();
    private int piezaBlanca;
    private boolean desordenado = false;
	
	public Model(int rowNum, int columnNum, int pieceSize, String[] imageList) {
		super(rowNum, columnNum, pieceSize, imageList);
		iconArray = new ArrayList<>();
		piezaBlanca = 0;
        for(int i =0; i< imageList.length; i++) {
        	iconArray.add(new PieceModel(i,(int)(i/columnNum), (int)(i%columnNum),pieceSize,imageList[i]));
        }
	}
	
	public Model(int rowNum, int columnNum, int pieceSize) {
		super(rowNum, columnNum, pieceSize);
		iconArray = new ArrayList<>();
		piezaBlanca = 0;
		int longitud = rowNum * columnNum;
		for(int i =0; i< longitud; i++) {
        	iconArray.add(new PieceModel(i,(int)(i/columnNum), (int)(i%columnNum),pieceSize));
        }
	}


	/*	Actualiza el intercambio de piezas, y comprueba si se ha ganado*/
	@Override
	public void update(int blankPos, int movedPos) {
		// TODO Auto-generated method stub
		PieceModel blanca = iconArray.get(piezaBlanca);
		PieceModel otraPieza = iconArray.get(movedPos);
    	
    	int xPieza = otraPieza.getIndexRow();
    	int yPieza = otraPieza.getIndexColumn();
    	
    	otraPieza.setColRow(blanca.getIndexColumn(), blanca.getIndexRow());
    	blanca.setColRow(yPieza,xPieza );
    	
	}

	/*	A�ade una pieza	*/
	@Override
	public void addNewPiece(int id, int indexRow, int indexCol, String imagePath) {
		// TODO Auto-generated method stub
		
        iconArray.add(new PieceModel(iconArray.size(),(int)(iconArray.size()%columnNum), (int)(iconArray.size()/columnNum),pieceSize,imageList[iconArray.size()]));
	}
	
	/*	A�ade una pieza	*/
	@Override
	public void addNewPiece(int id, int indexRow, int indexCol) {
		// TODO Auto-generated method stub
		iconArray.add(new PieceModel(iconArray.size(),(int)(iconArray.size()%columnNum), (int)(iconArray.size()/columnNum),pieceSize,""));
	}

	/*	Comprueba si se ha ganado	*/
	@Override
	public boolean isPuzzleSolve() {
		// TODO Auto-generated method stub
		int posicion=0;
		if(!desordenado) {
			for (int i=0; i<rowNum; i++) {
				for (int j=0; j<columnNum; j++) {
					PieceModel pieza = iconArray.get(posicion);
					
					if(pieza.getIndexColumn() != j || pieza.getIndexRow() != i) {
						return false;
					}
					posicion++;
				}
			}
			return true;
		}
		return false;
		
	}

	/*	Obtiene un movimiento aleatorio siempre y cuando sea
	 * 	posible el movimiento y no sea el movimiento que vuelve
	 * al estado anterior */
	@Override
	public int[] getRandomMovement(int lastPos, int pos) {
		
		boolean checkMovement = false;
		int [] newPosition = new int [2];
		PieceModel blanca = iconArray.get(piezaBlanca);
		
		
		while(!checkMovement) {
			
			int randomPosition = rnd.nextInt(4);
			
			if(randomPosition==0) {
				
				
				if(blanca.getIndexColumn()>0) {
					
					newPosition[0]=piezaBlanca;
					newPosition[1]=getPiecePosition(blanca.getIndexColumn()-1,blanca.getIndexRow());
					
					
					
					if(lastPos!=newPosition[1]) {
						checkMovement=true;
					}
					
					
				}
			}
			if(randomPosition==1) {
				
				
				if(blanca.getIndexRow()>0) {
					
					newPosition[0]=piezaBlanca;
					newPosition[1]=getPiecePosition(blanca.getIndexColumn(),blanca.getIndexRow()-1);
					
					if(lastPos!=newPosition[1]) {
						checkMovement=true;
					}
					
				}
			}
			if(randomPosition==2) {
				
				
				if(blanca.getIndexColumn()< columnNum-1) {
					
					newPosition[0]=piezaBlanca;
					newPosition[1]=getPiecePosition(blanca.getIndexColumn()+1,blanca.getIndexRow());
					
					if(lastPos!=newPosition[1]) {
						checkMovement=true;
					}
					
				}
			}
			if(randomPosition==3) {
				
				
				if(blanca.getIndexRow()<rowNum-1) {
					
					newPosition[0]=piezaBlanca;
					newPosition[1]=getPiecePosition(blanca.getIndexColumn(),blanca.getIndexRow()+1);

					if(lastPos!=newPosition[1]) {
						checkMovement=true;
					}
					
				}
			}
			
			
			
			
		}
		
		return newPosition;
	}
	
	/*	Obtiene la posici�n en el array de la pieza con la posici�n
	 * del tablero dada	*/
	public int getPiecePosition(int colum,int row){
		
		for(int i =0;i<iconArray.size();i++) {
			PieceModel piece = iconArray.get(i);
			if(piece.getIndexColumn()==colum && piece.getIndexRow()==row)
				return i;
		}
		return -1;
	}
	
	public void setDesordenado(boolean op) {
		desordenado=op;
	}

}

