package view;

import control.AbstractController;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import config.ConfigManagement;

import java.awt.*;
import java.io.File;

/**
 * Clase que representa la GUI principal.
 * @author Miguel Ã�ngel
 * @version 1.0
 */
public class PuzzleGUI extends JFrame{

    //Instancia singleton
    public static PuzzleGUI instance = null;
    //Controlador
    public static AbstractController controller;
    //NÃºmero de filas
    public static int rowNum=0;
    //NÃºmero de columnas
    public static int columnNum =0;
    //TamaÃ±o de imagen
    public static int imageSize =0;
    //Array de imagenes
    public static String[] imageList = null;
    //Tipo de base de datos
    //Panel de juego
    private BoardView boardView;
    


    /**
     * Constructor privado
     */
    private PuzzleGUI(){
        super("GMD PuzzleGUI");
        boardView = new BoardView(rowNum,columnNum,imageSize,imageList);
        boardView.addMouseListener(controller);
        this.getContentPane().setLayout(new BorderLayout());
        this.setJMenuBar(createMenuBar());
        this.getContentPane().add(boardView, BorderLayout.CENTER);
        this.getContentPane().add(createSouthPanel(), BorderLayout.SOUTH);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize((columnNum*imageSize)+110,(rowNum*imageSize)+(this.getJMenuBar().getComponent().getHeight())+100);
        //this.setSize(250, 250);
        setLocation(centerFrame());
    }

    //Singleton
    public static PuzzleGUI getInstance(){
        if(instance==null){
            instance = new PuzzleGUI();
        }
        return(instance);
    }
    
    /*Inicializa los parámetros*/
    public static void initialize(AbstractController controller, int rowNum,int columnNum,int imageSize,String[] imageList){
        PuzzleGUI.controller = controller;
        PuzzleGUI.rowNum = rowNum;
        PuzzleGUI.columnNum = columnNum;
        PuzzleGUI.imageSize = imageSize;
        PuzzleGUI.imageList = imageList;
    }
    
    /*Setea los parámetros */
    public void setParams (int rowNum,int columnNum,int imageSize,String[] imageList) {
    	PuzzleGUI.rowNum = rowNum;
        PuzzleGUI.columnNum = columnNum;
        PuzzleGUI.imageSize = imageSize;
        PuzzleGUI.imageList = imageList;
    }

    //MÃ©todo que crea el panel inferior
    private JPanel createSouthPanel(){
        JPanel southPanel = new JPanel();
        southPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        JButton clutterButton = new JButton("Desordenar");//botÃ³n de desordenar
        clutterButton.setActionCommand("clutter");
        
        JButton solveButton = new JButton("Resolver");
        solveButton.setActionCommand("solve");

        clutterButton.addActionListener(controller);
        solveButton.addActionListener(controller);


        southPanel.add(clutterButton);
        southPanel.add(solveButton);

        return(southPanel);
    }

    //MÃ©todo que genera la barra de menus
    private JMenuBar createMenuBar(){
        JMenuBar menu = new JMenuBar();
        JMenu archive = new JMenu("Archive");
        JMenu help = new JMenu("Help");
        
        JMenu config = new JMenu("Config");

        JMenuItem load = new JMenuItem("Load");
        load.setActionCommand("load");
        JMenuItem save = new JMenuItem("Save");
        save.setActionCommand("save");
        JMenuItem exit = new JMenuItem("Exit");
        exit.setActionCommand("exit");
        JMenuItem info = new JMenuItem("Info");
        info.setActionCommand("info");

        JMenuItem imgConfig = new JMenuItem("Img Config");
        config.setActionCommand("config");
        JMenuItem statistics = new JMenuItem("Statistics");
        config.setActionCommand("statistics");
        
        JMenu bdType = new JMenu("BD Mode");
        JMenuItem mongo = new JMenuItem("Mongo DB");
        bdType.setActionCommand("mongo");
        JMenuItem xml = new JMenuItem("XML");
        bdType.setActionCommand("xmlMode");
        bdType.add(mongo);
        bdType.add(xml);
        
        archive.add(load);
        archive.add(save);
        archive.add(exit);
        help.add(info);
        
        config.add(imgConfig);
        config.add(bdType);
        config.add(statistics);

        menu.add(archive);
        menu.add(help);
        
        menu.add(config);

        load.addActionListener(controller);
        exit.addActionListener(controller);
        info.addActionListener(controller);
        imgConfig.addActionListener(controller);
        save.addActionListener(controller);
        mongo.addActionListener(controller);
        xml.addActionListener(controller);
        statistics.addActionListener(controller);
        

        return(menu);
    }

    //Centrar el frame en el centro de la pantalla.
    private Point centerFrame(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int xCoord = (screenSize.width - this.getWidth()) / 2;
        int yCoord = (screenSize.height - this.getHeight()) / 2;
        return(new Point(xCoord,yCoord));
    }
    
    /*Muestra el selector de archivos y devuelve la imagen seleccionada*/
    public File showFileSelector(){
    	
        JFileChooser selector=new JFileChooser();
        File imagenSeleccionada = new File("");

        selector.setDialogTitle("Seleccione una imagen");

        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG & PNG", "jpg", "png");
        selector.setFileFilter(filtroImagen);

        int flag=selector.showOpenDialog(null);

        if(flag==JFileChooser.APPROVE_OPTION){
            try {
                //Devuelve el fichero seleccionado
                imagenSeleccionada=selector.getSelectedFile();
                return imagenSeleccionada;
            } catch (Exception e) {
            }
                  
        }

        return imagenSeleccionada;
    }

    public BoardView getBoardView(){
        return(this.boardView);
    }

    /*	MÃ©todo para actualizar la imagen del tablero
     * Obtenemos los parámetros de configuración restantes,  actualizamos
     * la configuración la vista del tablero y la configuración en la clase configuraciones
     */
    public void updateBoard(File imageFile){
    	
    	rowNum = Integer.parseInt(JOptionPane.showInputDialog("Introduzca Filas"));
		columnNum = Integer.parseInt(JOptionPane.showInputDialog("Introduzca Columnas"));
		imageSize = Integer.parseInt(JOptionPane.showInputDialog("Introduzca Tamaño Casilla"));
		
    	
    	
    	controller.removeObserver(boardView);
    	this.remove(boardView);
    	boardView = new BoardView(rowNum, columnNum, imageSize, imageFile);
    	
    	boardView.addMouseListener(controller);
    	this.getContentPane().add(boardView, BorderLayout.CENTER);
    	this.revalidate();
    	this.getGraphics().clearRect(0,0,imageSize*rowNum,imageSize*columnNum);
    	this.setSize((columnNum*imageSize)+110,(rowNum*imageSize)+(this.getJMenuBar().getComponent().getHeight())+100);
    	update(this.getGraphics());
    	controller.addObserver(boardView);
    	ConfigManagement.setConfiguration(rowNum, columnNum, imageSize, imageList);
    }
    
    /*	Actualiza la configuración de la vista del tablero, para cuando cargamos
     * una partida guardada anteriormente							*/
    public void updateBoardLoad() {
    	controller.removeObserver(boardView);
    	this.remove(boardView);
    	boardView = new BoardView(rowNum, columnNum, imageSize, imageList);
    	boardView.addMouseListener(controller);
    	this.getContentPane().add(boardView, BorderLayout.CENTER);
    	this.revalidate();
    	this.getGraphics().clearRect(0,0,imageSize*rowNum,imageSize*columnNum);
    	this.setSize((columnNum*imageSize)+110,(rowNum*imageSize)+(this.getJMenuBar().getComponent().getHeight())+100);
    	update(this.getGraphics());
    	controller.addObserver(boardView);
    }


    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
    
    /*	Crea una ventana bloqueante con el mensaje pasado por parámetros*/
    public void messagePanel (String cadena) {
    	JOptionPane.showMessageDialog(instance.getContentPane(), cadena) ;
    }
    
    public int[] GetPanelSize() {
    	
    	int [] pos = {this.getContentPane().getComponent(0).getHeight(), this.getContentPane().getComponent(0).getWidth()};
    	return pos;
    	
    }

}
