package view;

import javax.swing.*;
import java.awt.*;

/**
 * Clase que representa la vista del tablero
 * @author Miguel Ã�ngel
 * @version 1.0
 */
public class PieceView extends ImageIcon implements Cloneable{

    //id de la imagen
    private int id;
    //Ã­ndice de fila
    private int indexRow;
    //Ã­ndice de columna
    private int indexColumn;
    //TamaÃ±o de la imagen
    private int imageSize;


    /**
     * Constructor de una clase
     * @param indexRow indice de fila
     * @param indexColumn indice de columna
     * @param imagePath ubicaciÃ³n de la imagen.
     */
    public PieceView(int id,int indexRow, int indexColumn,int imageSize,String imagePath){
        super(imagePath);
        this.id = id;
        this.indexRow = indexRow;
        this.indexColumn = indexColumn;
        this.imageSize = imageSize;
    }

    public PieceView(int id, int indexRow, int indexColumn,int imageSize,Image image){
        super(image);
        this.id = id;
        this.indexRow = indexRow;
        this.indexColumn = indexColumn;
        this.imageSize = imageSize;
    }


    public int getIndexRow() {
        return indexRow;
    }
    
    public void setIndexRow(int row) {
    	this.indexRow = row;
    }

    public int getIndexColumn() {
        return indexColumn;
    }
    
    public void setIndexColumn(int col) {
    	this.indexColumn = col;
    }
    
    public void setColRow (int col, int row) {
    	setIndexRow(row);
    	setIndexColumn(col);
    }
    
    public int getDrawnRowIndex() {
    	return indexRow * imageSize;
    }
    public int getDrawnColumnIndex() {
    	return indexColumn * imageSize;
    }
    
    public int getImageSize() {
        return imageSize;
    }

    public void setImageSize(int imageSize) {
        this.imageSize = imageSize;
    }

    public int getId(){
        return this.id;
    }

    public String toString(){
        return("id:"+id);
    }

}
