package view;

import observer.Observer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Clase que representa la vista del tablero
 * @author Miguel Ã�ngel
 * @version 1.0
 */
public class BoardView extends JPanel implements Observer {
    private ArrayList<PieceView> iconArray = null;
    private int columnSize;
    private int rowSize;
    private int imageSize;
    public static int imageWidth= 96;
    public static int imageHeight= 96;
    private int piezaBlancaPosicion;
    
    /*	Constructor 1*/
    public BoardView(int rowNum, int columnNum,int imageSize, String[] imageList){
        super();
        iconArray = new ArrayList<>();
        columnSize = columnNum;
        rowSize = rowNum;
        this.imageSize = imageSize;
        imageWidth= imageSize*columnSize;
        imageHeight= imageSize*rowSize;
        piezaBlancaPosicion = 0;
        this.imageSize = imageSize;
        for(int i =0; i< imageList.length; i++) {
        	iconArray.add(new PieceView(i,(int)(i/columnNum), (int)(i%columnNum),imageSize,imageList[i]));
        }

    }
    
    /*	Constructor 2*/
    public BoardView(int rowNum, int columnNum, int imageSize, File imageFile){
        super();
        iconArray = new ArrayList<>();
        columnSize = columnNum;
        rowSize = rowNum;
        imageWidth= imageSize*columnSize;
        imageHeight= imageSize*rowSize;
        piezaBlancaPosicion = 0;
        this.imageSize = imageSize;
        
        BufferedImage resizedImage = resizeImage(imageFile);
        BufferedImage[] bufferedImageList = splitImage(resizedImage);
        
        for(int i =0; i< bufferedImageList.length; i++) {
        	iconArray.add(new PieceView(i,(int)(i/columnNum), (int)(i%columnNum),imageSize,bufferedImageList[i]));
        }
    }

    //redimensionamos la imagen para 96*96
    private BufferedImage resizeImage(File fileImage){
        BufferedImage resizedImage = null;
        try {
			resizedImage = ImageIO.read(fileImage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Image newImage = resizedImage.getScaledInstance(columnSize*imageSize, rowSize*imageSize, Image.SCALE_DEFAULT);
        resizedImage = new BufferedImage(newImage.getWidth(null), newImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = resizedImage.createGraphics();
        bGr.drawImage(newImage, 0, 0, null);
        bGr.dispose();
        
        return(resizedImage);
    }

    //dividimos la imagen en el nÃºmero
    private BufferedImage[] splitImage(BufferedImage image){
        //Divisor de imÃ¡genes
    	String[] listImages = new String[rowSize*columnSize];
    	BufferedImage images[] = new BufferedImage[rowSize*columnSize];
    	try {
			ImageIO.write(image, "jpg", new File("yeee.jpg"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	int contador = 0;
    	for (int i=0; i<rowSize; i++) {
    		for (int j=0; j<columnSize; j++) {
    			images[contador] =image.getSubimage(j*imageSize, i*imageSize, imageSize, imageSize);
    			
    			try {
    				ImageIO.write(images[contador], "jpg", new File(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"pieza"+contador+".jpg"));
    				listImages[contador]=System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"pieza"+contador+".jpg";
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			contador++;
    		}
    	}
    	String blancaPath=System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"blanco.jpg";
    	
    	BufferedImage blanca;
		try {
			blanca = ImageIO.read(new File(blancaPath));
			images[piezaBlancaPosicion] =  blanca.getSubimage(0, 0, imageSize, imageSize);
			ImageIO.write(images[piezaBlancaPosicion],"jpg",new File(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"piezablanca.jpg"));
			listImages[piezaBlancaPosicion] = System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"piezablanca.jpg";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PuzzleGUI.getInstance().imageList =  listImages;
        return(images);
    }

    /*	Actualiza el intercambio de piezas*/
    public void update(int blankPos, int movedPos){
    	PieceView blanca = iconArray.get(piezaBlancaPosicion);
    	PieceView otraPieza = iconArray.get(movedPos);
    	
    	int xPieza = otraPieza.getIndexColumn();
    	int yPieza = otraPieza.getIndexRow();
    	
    	otraPieza.setColRow(blanca.getIndexColumn(), blanca.getIndexRow());
    	blanca.setColRow(xPieza, yPieza);
    	this.getGraphics().clearRect(0,0,imageWidth,imageHeight);
    	update(this.getGraphics());
    	
    }

    public void update(Graphics g){
        paint(g);
    }

    public void paint(Graphics g){
        for(PieceView iconImage:iconArray){
            g.drawImage(iconImage.getImage(), iconImage.getDrawnColumnIndex(), iconImage.getDrawnRowIndex(), iconImage.getImageSize(), iconImage.getImageSize(), this);
        }
    }

    //Dado una posicion X e Y localizar una pieza
    private int locatePiece(int posX,int posY){
    	int casilla;
    	
    	casilla = (posX/imageSize) + ((posY/imageSize))*columnSize;

        return(casilla);
    }

    /**
     * Mueve la pieza y devuelve las coordenadas en un array de dos posiciones
     * donde: la primera posicion representa la posicion actual de la pieza blanca
     * y la segunda posicion representa la posicion actual de la pieza a mover.
     * @param posX posicion X del puntero
     * @param posY posicion Y del puntero.
     * @return Array de dos posiciones: posicion actual de la pieza blanca y posicion
     * actual de la pieza que tiene que ser movida.
     */
    public int[] movePiece(int posX,int posY){
    	int []posiciones = new int [2];

    	if (posX <= imageWidth && posY <= imageHeight) {

	    	int otraPieza = locatePiece(posX,posY);
	    	int xPieza = otraPieza / columnSize;
	    	int yPieza = otraPieza - columnSize*xPieza;
	    	int OtraPiezaPosicion = buscarPieza(xPieza,yPieza);
	    	
	    	PieceView blanca = iconArray.get(piezaBlancaPosicion);
	    	int yPiezaBlanca = blanca.getIndexColumn();
	    	int xPiezaBlanca = blanca.getIndexRow();
	    	
	    	if (sonVecinos(xPiezaBlanca,yPiezaBlanca,xPieza,yPieza)) {
	    		posiciones[0] = piezaBlancaPosicion;
	    		posiciones[1] = OtraPiezaPosicion;
	    	}
	    	else {
	    		posiciones[0] = -1;
	    		posiciones[1] = -1;
	    	}
    	}
    	else {
    		posiciones[0] = -1;
    		posiciones[1] = -1;
    	}
    	return posiciones;
    }
    
    //Devuelve la posición de la pieza en el array inconArray según la columna y la fila en el tablero
    private int buscarPieza(int col, int fil) {
    	for(int i = 0; i<iconArray.size();i++ ) {
    		PieceView pieza = iconArray.get(i); 
    		if(col == pieza.getIndexRow() && fil == pieza.getIndexColumn()) {
    			return i;
    		}
    	}
    	return -1;
    }
    
    //Comprueba si la blanca y la otra son colindantes
    private boolean sonVecinos (int xPiezaBlanca, int yPiezaBlanca, int xPieza, int yPieza) {
    	int distanciax = Math.abs(xPieza - xPiezaBlanca);
    	int distanciay = Math.abs(yPieza - yPiezaBlanca);
    	int distancia = distanciax + distanciay;
    	
    	if(distancia == 1) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    

}
