package config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;


import command.Command;
import command.ConcreteCommand;

//import view.PuzzleGUI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.InputSource;

//Clase que se encarga de manejar el cargado y guardado de configuraciones,
//incluyendo la carga de la configpuraci�n inicial
public class ConfigManagement {

	private static Config con;
	private static int filas;
	private static int columnas;
	private static int size;
	private static String[] url;
	private static int basedatos;
	
	final static int NONE = 0;
	final static int MONGO = 1;
	final static int XML = 2;
	
	/*	Funci�n para guardar la configuraci�n de la partida en un Json.
	 * 
	 *	Crea un objeto de la clase SaveGame en el que coloca la lista de comandos, el
	 *	maximo de columnas, el maximo de filas, el tama�o de la casilla y las rutas de las imagenes,
	 *  para posteriormente parsearlo a Json y guardarlo en un archivo .Json*/
	public static void guardarConfiguracion(LinkedList<Command> comandos) {
		
		LinkedList<ConcreteCommand> auxList = new LinkedList<ConcreteCommand>();
		for (int i = 0; i<comandos.size();i++) {
			auxList.add((ConcreteCommand) comandos.get(i));
		}
		
		SaveGame partida = new SaveGame(auxList,columnas,filas,size,url);
		try (Writer writer = new FileWriter(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"savegame.json")) {
		    Gson gson = new GsonBuilder().setPrettyPrinting().create();
		    String salida = gson.toJson(partida);
		    BufferedWriter archivo = new BufferedWriter(writer);
		    archivo.write(salida);
		    archivo.close();	    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*	Funci�n para cargar la configuraci�n de la partida desde un Json.
	 *
	 *	Crea un objeto de la clase SaveGame en el que coloca la lista de comandos, el
	 *	maximo de columnas, el maximo de filas, el tama�o de la casilla y las rutas 
	 *	de las imagenes obtenidas desde el Json, seteando la configuraci�n en par�metros
	 *	y devolviendo la lista de comandos a la clase Controller					 */
	public static LinkedList<Command> cargarPartida() {
		BufferedReader reader;
		LinkedList <Command> list = new LinkedList<Command>();
		try {
			reader = new BufferedReader(new FileReader(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"savegame.json"));
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
		    
			SaveGame json = gson.fromJson(reader, SaveGame.class);
			LinkedList <ConcreteCommand> auxlist = json.getCommandsList();
			
			for (int i = 0; i<auxlist.size();i++) {
				list.add( auxlist.get(i));
			}
			setConfiguration(json.getRowNum(),json.getColumnNum(),json.getSizeImage(),json.getImageList(),"none");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/*	Funci�n que lee la configuraci�n inicial del archivo config.xml
	 * 	y setea la configuraci�n en los par�metros. Devuelve el objeto Config	*/
	public static Config getConfiguration() {
		Config configuration = new Config();
		try {
			System.out.println(validateWithDTDUsingDOM(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"config.xml"));;
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		File fichero = new File(System.getProperty("user.dir")+System.getProperty("file.separator")+"resources"+System.getProperty("file.separator")+"config.xml");
		try {
			JAXBContext contexto = JAXBContext.newInstance(Config.class);
			Unmarshaller marshal = contexto.createUnmarshaller();
			configuration = (Config) marshal.unmarshal(fichero);
			con = configuration;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setConfiguration(con.getNumFilas(),con.getNumColum(),con.getImageSize(),completarDir(),con.getBaseDatos());
		return con;
	}
	
	/*Obtiene las rutas desde el archivo xml y las separa */
	public static String[] completarDir() {
		String imagePath=System.getProperty("user.dir");
        String dir = con.getDirec();
        String[] imageList= dir.split(",");
        for(int i=0;i<=imageList.length-1;i++) {
        	imageList[i] = imagePath + imageList[i];
        }
        return imageList;
	}
	
	
	public static int getFilas() {
		return filas;
	}

	public static void setFilas(int filas) {
		ConfigManagement.filas = filas;
	}

	public static int getColumnas() {
		return columnas;
	}

	public static void setColumnas(int columnas) {
		ConfigManagement.columnas = columnas;
	}

	public static int getSize() {
		return size;
	}

	public static void setSize(int size) {
		ConfigManagement.size = size;
	}

	public static String[] getUrl() {
		return url;
	}

	public static void setUrl(String[] url) {
		ConfigManagement.url = url;
	}
	
	public static void setBaseDatos(int num) {
		basedatos = num;
	}

	public static int getBaseDatos() {
		return basedatos;
	}

	//Seteamos la configuraci�n inicial del Xml de configuraci�n incial
	private static void setConfiguration(int row, int col, int imageSize, String[] pathList , String base) {
		filas = row;
		columnas = col;
		size = imageSize;
		url = pathList;
		switch(base) {
			case "none":
				basedatos = NONE;
				break;
			case "mongo":
				basedatos = MONGO;
				break;
			case "xml":
				basedatos = XML;
				break;
			default:
				System.out.println("EL tipo de base de datos no es correcto");
				break;
		}		
	}
	
	//Si cargamos una imagen con otras filas y columnas, mantenemos la base de datos anterior
	public static void setConfiguration(int row, int col, int imageSize, String[] pathList ) {
		filas = row;
		columnas = col;
		size = imageSize;
		url = pathList;		
	}
	
	//Seteamos la configuracion y la el modo de base de datos
	public static void setConfiguration(int row, int col, int imageSize, String[] pathList, int base ) {
		filas = row;
		columnas = col;
		size = imageSize;
		url = pathList;	
		basedatos = base;
	}
	
	/* Funci�n para validar el xml de configuraci�n inicial con el DTD*/
	public static boolean validateWithDTDUsingDOM(String xml) 
		    throws ParserConfigurationException, IOException
	{
	    try {
	      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	      factory.setValidating(true);
	      factory.setNamespaceAware(true);

	      DocumentBuilder builder = factory.newDocumentBuilder();

	      builder.setErrorHandler(
	          new ErrorHandler() {
	            public void warning(SAXParseException e) throws SAXException {
	              System.out.println("WARNING : " + e.getMessage()); // do nothing
	            }

	            public void error(SAXParseException e) throws SAXException {
	              System.out.println("ERROR : " + e.getMessage());
	              throw e;
	            }

	            public void fatalError(SAXParseException e) throws SAXException {
	              System.out.println("FATAL : " + e.getMessage());
	              throw e;
	            }
	          }
	          );
	      builder.parse(new InputSource(xml));
	      return true;
	    }
	    catch (ParserConfigurationException pce) {
	      throw pce;
	    } 
	    catch (IOException io) {
	      throw io;
	    }
	    catch (SAXException se){
	      return false;
	    }
	}
}
