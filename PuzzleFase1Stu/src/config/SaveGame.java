package config;

import java.util.LinkedList;
import command.ConcreteCommand;

/*	Clase necesaria para guardar y cargar partida en json*/
public class SaveGame {
	LinkedList<ConcreteCommand> commandsList;
	int columnNum;
	int rowNum;
	int sizeImage;
	String[] imageList;
	public SaveGame(LinkedList<ConcreteCommand> commandsList, int columnNum, int rowNum, int sizeImage, String[] imageList) {
		super();
		this.commandsList = commandsList;
		this.columnNum = columnNum;
		this.rowNum = rowNum;
		this.sizeImage = sizeImage;
		this.imageList = imageList;
	}
	public LinkedList<ConcreteCommand> getCommandsList() {
		return commandsList;
	}
	public void setCommandsList(LinkedList<ConcreteCommand> commandsList) {
		this.commandsList = commandsList;
	}
	public int getColumnNum() {
		return columnNum;
	}
	public void setColumnNum(int columnNum) {
		this.columnNum = columnNum;
	}
	public int getRowNum() {
		return rowNum;
	}
	public void setRowNum(int rowNum) {
		this.rowNum = rowNum;
	}
	public int getSizeImage() {
		return sizeImage;
	}
	public void setSizeImage(int sizeImage) {
		this.sizeImage = sizeImage;
	}
	public String [] getImageList() {
		return imageList;
	}
	public void setImageList(String []imageList) {
		this.imageList = imageList;
	}
}

