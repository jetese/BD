package config;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

//Clase para importar la configuración inicial del xml
@XmlRootElement(name = "config") 
public class Config{
	
	private int filas;
	private int columnas;
	private int size;
	private String direc;
	private String base;
    public Config(){}
    
    
    @XmlElement(name = "filas")
	public int getNumFilas() {
		return filas;
	}
	
	public void setNumFilas(int filas) {
		this.filas = filas;
	}
	@XmlElement(name = "columnas")
	public int getNumColum() {
		return columnas;
	}

	public void setNumColum(int columnas) {
		this.columnas = columnas;
	}
	@XmlElement(name = "size")
	public int getImageSize() {
		return size;
	}

	public void setImageSize(int size) {
		this.size = size;
	}
	
	@XmlElement(name = "direc")
	public String getDirec() {
		return direc;
	}

	public void setDirec(String direc) {
		this.direc = direc;
	}

	@XmlElement(name = "base")
	public String getBaseDatos() {
		// TODO Auto-generated method stub
		return base;
	}
	public void setBaseDatos(String base) {
		this.base = base;
	}
	
	
	

}
